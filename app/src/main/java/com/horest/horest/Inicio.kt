package com.horest.horest

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_inicio.*


class Inicio : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)
        val tvUsuario: TextView = findViewById(R.id.NameUs)
        val salir: TextView = findViewById(R.id.salir)
        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tvUsuario.text = usuario

        salir.setOnClickListener {
            if (usuario == "Google") {

                Firebase.auth.signOut()
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, Login::class.java))
                finish()
            }
            startActivity(Intent(this, Login::class.java))
        }

        val hoteles: CardView = findViewById(R.id.hoteles)
        val restaurante: CardView = findViewById(R.id.restaurante)

        hoteles.setOnClickListener {
            val i = Intent(Intent(this, MenuTipoH::class.java))
            i.putExtra("USUARIO", usuario)
            startActivity(i)
        }
        restaurante.setOnClickListener {
            val i = Intent(Intent(this, MenuTipoR::class.java))
            i.putExtra("USUARIO", usuario)
            startActivity(i)
        }
        creditos1.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }
    }

}